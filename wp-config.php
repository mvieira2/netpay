<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'netpay' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'sj7j5jk@SFruh|wZf++8Usiv~_KGMzcAHExN{v~|=#qv:J+[ls|n2c;u_qsOyg0<' );
define( 'SECURE_AUTH_KEY',  'Hn:(#i+1PKDO!VA[Teqevy/E$~9VAcZ6a6h`HmGDII+Hnj@vy{7N^t#X7z$Nt*NC' );
define( 'LOGGED_IN_KEY',    '{ 6IEyKJi2@BsEg!{T*VNU%C_wUW~2aPv ibIiuWINQ4&v(+JL7IYFfd4:< {|yS' );
define( 'NONCE_KEY',        '&c|ZXqt!]Un]uIOd<*~+kNFcJag#Ykg`SBGwJ^#$X(Yz0nkB^DUeyi?O&l@GS?bz' );
define( 'AUTH_SALT',        'H^(<b@AP~F_F;U!ez-;76.i5yS@:sl<,05uvlV}Ar?GFd@8EVPl8.EFe7)[}1|,1' );
define( 'SECURE_AUTH_SALT', 'h;OX-]sU-g&blSetsK~cDS-%s^FgONF7z7})=g68]CDN_e2R|WpYJ:mcxh_5YHDj' );
define( 'LOGGED_IN_SALT',   'gM: V[YV.`cRb@*lBOEx;v**|hts(G0A3xfX|Zan&t~59Y4@s)%B4)g!}/xM)rZ4' );
define( 'NONCE_SALT',       '<=|2Iw==W,)l6Z)MXJ2H{G=.DxX$^OkbfAc.>^T.{`N>Vc7!6?G`nvJpK3LS,feW' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

<?php get_header(); ?>

<main>
    <?php if (!is_home() && !is_front_page()) : ?>
        <div class="banner-title-page" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/banner4.png') ">
            <h1><?php the_title(); ?></h1>
        </div>

    <?php endif; ?>
    <br><br>

    <div class="container">
        <div class="row form-contato">
            <div class="col-sm-7">
                <?php get_template_part('loop-page'); ?>
            </div>
            <div class="col-sm-5">
                <?php
                echo do_shortcode(' [contact-form-7 title="Contact form 1"]');
                ?>
            </div>
        </div>
    </div>



</main>


<?php get_footer(); ?>
<?php get_header(); ?>
<main>


    <section>

        <?php
        echo do_shortcode('[smartslider3 slider=1]');
        ?>

    </section>

    <section>
        <div class="row">
            <div class="col-sm-7">
                <img src="<?php echo get_template_directory_uri(); ?>/img/banner2.png">
            </div>
            <div class="col-sm-5">
                <div class="box-33  padding-mobile">
                    <h2 class="title-1 ">Padrão mundial de segurança</h2>
                    <p>
                        Somos padrão mundial de segurança PCI DSS, <br>
                        oferecemos uma plataforma totalmente segura através <br>
                        do processamento e tráfego criptografado de todos os<br>
                        dados dos cartões. Temos parâmetros rigorosos e <br>
                        flexíveis para monitoração de fraudes, além de <br>
                        uma equipe dedicada para atender nossos parceiros.

                    </p>
                </div>
            </div>
        </div>

    </section>
    <section>
        <div class="container">
            <div class="row container-fluid box-1">
                <div class="col-sm-12">
                    <h2 class="title-2 text-center">Perfeita para todos os tipos de negócios</h2>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                    <div class="col-xs-6">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/img1.png">
                    <p>
                        Universidades, <br>
                        escolas, creches, <br>
                        clubes, academias, <br>
                        escolas de cursos
                    </p>

                </div>
                <div class="col-xs-6">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/img4.png">
                    <p>
                        Imobiliárias, <br>
                        corretoras, <br>
                        incorporadoras, <br>
                        administradoras
                    </p>

                </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                    <div class="col-xs-6">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/img3.png">
                    <p>
                        Clínicas, <br>
                        consultórios, <br>
                        dentistas, <br>
                        médicos
                    </p>

                </div>
                <div class="col-xs-6">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/img2.png">
                    <p>
                        Escritórios de <br>
                        contabilidade, <br>
                        advocacia e <br>
                        serviços gerais
                    </p>

                </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row ">
            <div class="col-sm-12">
                <h2 class="title-2 text-center">Nossos produtos</h2>
            </div>
            <div class="col-sm-12 box-4">
                <div class="bg-cinza"></div>

                <div class="container">
                    <div class="row text-center">
                        <div class="col-sm-4">
                            <div class="box-white">
                                <h3>Pagamentos <br> à vista</h3>
                                <p>
                                    Permite aos estabelecimentos <br>
                                    comerciais oferecer a seus <br>
                                    clientes uma forma rápida e <br>
                                    segura, online para pagamentos
                                    <br> com cartão de crédito à vista.
                                </p>
                            </div>
                            <a href="/servicos#aba1" class="botao-4"> saiba mais</a>
                        </div>
                        <div class="col-sm-4">
                            <div class="box-white">
                                <h3>Pagamentos <br> Recorrentes</h3>
                                <p>
                                    Fazemos todo o gerenciamento <br>
                                    das cobranças ao portador <br>
                                    do cartão, periodicamente, <br>
                                    acionando o estabelecimento <br>
                                    comercial apenas para realizar <br>
                                    os créditos das transações.
                                </p>
                            </div>
                            <a href="/servicos#aba2" class="botao-4"> saiba mais</a>
                        </div>
                        <div class="col-sm-4">
                            <div class="box-white">
                                <h3>Pagamentos <br> Parcelados</h3>
                                <p>
                                    Permite aos estabelecimentos <br>
                                    comerciais oferecer a seus <br>
                                    clientes o parcelamento <br>
                                    sem juros dos serviços em <br>
                                    até 12 vezes.
                                </p>
                            </div>
                            <a href="/servicos#aba3" class="botao-4"> saiba mais</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <?php get_template_part('contents/principais-parceiros'); ?>

</main>
<?php get_footer(); ?>
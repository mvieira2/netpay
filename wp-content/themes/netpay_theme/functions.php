<?php

define('THEME_VERSION', '1.0.0.9');

require_once get_template_directory() . '/class/tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
function my_theme_register_required_plugins()
{

    $plugins = array(

        array(
            'name'               => 'Bold Builder', 
            'slug'               => 'bold-page-builder', 
            'source'             => get_stylesheet_directory() . '/plugins/bold-page-builder.zip', 
            'required'           => true, 
            'version'            => '', 
            'force_activation'   => true, 
            'force_deactivation' => false, 
            'external_url'       => '', 
            'is_callable'        => '', 
        ),

        array(
            'name'               => 'Bootstrap for Contact Form 7', 
            'slug'               => 'bootstrap-for-contact-form-7', 
            'source'             => get_stylesheet_directory() . '/plugins/bootstrap-for-contact-form-7.zip', 
            'required'           => true, 
            'version'            => '', 
            'force_activation'   => true, 
            'force_deactivation' => false, 
            'external_url'       => '', 
            'is_callable'        => '', 
        ),

        array(
            'name'               => 'Contact Form 7', 
            'slug'               => 'contact-form-7', 
            'source'             => get_stylesheet_directory() . '/plugins/contact-form-7.zip', 
            'required'           => true, 
            'version'            => '', 
            'force_activation'   => true, 
            'force_deactivation' => false, 
            'external_url'       => '', 
            'is_callable'        => '', 
        ),

        array(
            'name'               => 'Smart Slider 3', 
            'slug'               => 'smart-slider-3', 
            'source'             => get_stylesheet_directory() . '/plugins/smart-slider-3.zip', 
            'required'           => true, 
            'version'            => '', 
            'force_activation'   => true, 
            'force_deactivation' => false, 
            'external_url'       => '', 
            'is_callable'        => '', 
        ),



    );


    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.

        
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'theme-slug' ),
            'menu_title'                      => __( 'Install Plugins', 'theme-slug' ),
            // translators: %s: plugin name.
            'installing'                      => __( 'Installing Plugin: %s', 'theme-slug' ),
            // translators: %s: plugin name.
            'updating'                        => __( 'Updating Plugin: %s', 'theme-slug' ),
            'oops'                            => __( 'Something went wrong with the plugin API.', 'theme-slug' ),
            'notice_can_install_required'     => _n_noop(
                // translators: 1: plugin name(s).
                'This theme requires the following plugin: %1$s.',
                'This theme requires the following plugins: %1$s.',
                'theme-slug'
            ),
            'notice_can_install_recommended'  => _n_noop(
                // translators: 1: plugin name(s).
                'This theme recommends the following plugin: %1$s.',
                'This theme recommends the following plugins: %1$s.',
                'theme-slug'
            ),
            'notice_ask_to_update'            => _n_noop(
                // translators: 1: plugin name(s).
                'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
                'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
                'theme-slug'
            ),
            'notice_ask_to_update_maybe'      => _n_noop(
                // translators: 1: plugin name(s).
                'There is an update available for: %1$s.',
                'There are updates available for the following plugins: %1$s.',
                'theme-slug'
            ),
            'notice_can_activate_required'    => _n_noop(
                // translators: 1: plugin name(s).
                'The following required plugin is currently inactive: %1$s.',
                'The following required plugins are currently inactive: %1$s.',
                'theme-slug'
            ),
            'notice_can_activate_recommended' => _n_noop(
                // translators: 1: plugin name(s).
                'The following recommended plugin is currently inactive: %1$s.',
                'The following recommended plugins are currently inactive: %1$s.',
                'theme-slug'
            ),
            'install_link'                    => _n_noop(
                'Begin installing plugin',
                'Begin installing plugins',
                'theme-slug'
            ),
            'update_link'                     => _n_noop(
                'Begin updating plugin',
                'Begin updating plugins',
                'theme-slug'
            ),
            'activate_link'                   => _n_noop(
                'Begin activating plugin',
                'Begin activating plugins',
                'theme-slug'
            ),
            'return'                          => __( 'Return to Required Plugins Installer', 'theme-slug' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'theme-slug' ),
            'activated_successfully'          => __( 'The following plugin was activated successfully:', 'theme-slug' ),
            // translators: 1: plugin name.
            'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'theme-slug' ),
            // translators: 1: plugin name.
            'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'theme-slug' ),
            // translators: 1: dashboard link.
            'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'theme-slug' ),
            'dismiss'                         => __( 'Dismiss this notice', 'theme-slug' ),
            'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'theme-slug' ),
            'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'theme-slug' ),

            'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
        ),

);

tgmpa( $plugins, $config );

}



if ( ! file_exists( get_template_directory() .  '/class/class-wp-bootstrap-navwalker.php' ) ) {

    return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
    require_once get_template_directory() . '/class/class-wp-bootstrap-navwalker.php';
}  



if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); 



    // Add Support for Custom Header - Uncomment below if you're going to use
    add_theme_support('custom-header', array(
        'default-image'         => get_template_directory_uri() . '/img/logo.png',
        'header-text'           => false,
        'default-text-color'        => '000',
        //'width'             => 1000,
        //'height'            => 198,
        //'random-default'        => false,
        //'wp-head-callback'      => $wphead_cb,
        //'admin-head-callback'       => $adminhead_cb,
        //'admin-preview-callback'    => $adminpreview_cb
    ));

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}


function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'footer-menu' => __( 'Footer Menu' ),
      'footer-menu-2' => __( 'Footer Menu 2' ),
  )
);
}
add_action( 'init', 'register_my_menus' );


function menu($name ='', $class = '')
{
   return wp_nav_menu(
     array(
        'theme_location'  => $name,
        //'depth'             => 2,
        'container'         => 'ul',
        'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'bs-example-navbar-collapse-'.$name,
        'menu_class'        => $class,
        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
        'walker'            => new WP_Bootstrap_Navwalker(),
    )
 );
}

add_action('after_setup_theme', function () {
    add_theme_support('starter-content', [
        // Starter pages to include
        'posts' => [
            'home',
            'about',
            'contact',
            'netpay-no-mundo',

        ],
        // Add pages to primary navigation menu
        'nav_menus' => [
            'header-menu' => [
                'name' => __('Header Menu', 'id'),
                'items' => [
                    'home_link',
                    'page_about',
                    'page_blog',
                    'page_contact',
                ],
            ],
            'footer-menu' => [
                'name' => __('Footer Menu', 'id'),
                'items' => [
                    'page_netpay-no-mundo',
                ],
            ],
            'footer-menu-2' => [
                'name' => __('Footer Menu', 'id'),
                'items' => [
                    'page_netpay-no-mundo',
                ],
            ],
        ],

    ]);
});

function my_rand()
{
    return THEME_VERSION;
}
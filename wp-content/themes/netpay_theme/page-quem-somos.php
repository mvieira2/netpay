<?php get_header(); ?>

<main>
    <?php if (!is_home() && !is_front_page()) : ?>
        <div class="banner-title-page" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/banner4.png') ">
            <h1><?php the_title(); ?></h1>
        </div>

    <?php endif; ?>
    <br><br>
    <div class="text-center">
        <?php get_template_part('loop-page'); ?>
    </div>
    <br><br>
    <div class="container">
        <?php get_template_part('contents/principais-parceiros'); ?>
    </div>



</main>


<?php get_footer(); ?>
<footer>

	<div class="box-3 box-33 padding-mobile">


		<h2 class="text-center title-3">Fique por dentro das novidades</h2>

		<?php
		echo do_shortcode('[contact-form-7 title="Newsletters"]');
		?>



	</div>
<div class="clearfix"></div>
	<div class="container">
		<div class="row container-fluid">
			<div class="col-sm-12">
				<h3>Entre em contato</h3>
			</div>
			<div class="col-sm-4">
				<ul>
					<li>
						<strong>
							E-mail
						</strong>
						<br />
						<a href="mailto:comercial@netpaybrasil.com.br" target="_blank">
							comercial@netpaybrasil.com.br
						</a>
					</li>
					<li>
						<strong>
							Telefone
						</strong>
						<br />
						<a href="tel:+551135871216" target="_blank">
							+55 (11) 3587-1216
						</a>
					</li>
					<li>
						<strong>
							Endereço
						</strong>
						<br />
						<a href="https://goo.gl/maps/j17CNexGsF7wTEE69" target="_blank">
							Alameda Santos, 211 cj.508
						</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-4">
				<?php menu('footer-menu', ''); ?>
			</div>
			<div class="col-sm-4 ">
				<div class="row rede-sociais">
					<div class="col-sm-12">
						<strong>Redes Sociais</strong>
					</div>
					<div class="col-xs-4">
						<a href="https://www.facebook.com/netpaybrasil" target="_blank">
							<img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="Facebook">
						</a>
					</div>
					<div class="col-xs-4">
						<a href="https://www.instagram.com/netpaybrasil/" target="_blank">
							<img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png" alt="Instagram">

						</a>

					</div>
					<div class="col-xs-4">
						<a href="https://www.linkedin.com/company/netpay-international/" target="_blank">
							<img src="<?php echo get_template_directory_uri(); ?>/img/likedin.png" alt="Linkedin">

						</a>

					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<p class="end">
							Netpay Brasil - <br>
							Facilitadora de Pagamentos Ltda. <br>
							CNPJ 30.058.140/001-81
						</p>
						<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=QbMb919kZNoylaLkWHpm4C7TtbxvEUMiQa20N1PfoiJeUfyVJoBv5WtL6oZX"></script></span>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-bottom">

			<div class="row container-fluid">
				<div class="col-sm-2">
					<a href="https://www.netpay-intl.com/" target="_blank" class="logo-sub-footer">
						<img class="" src="<?php echo get_template_directory_uri(); ?>/img/logo2.png">

					</a>
					
				</div>
				<div class="col-sm-10">
					<ul class="menu-footer-bottom mobile">

						<li class="current_page_item">
							<a>Netpay no Mundo ></a>
						</li>

					</ul>
					<?php menu('footer-menu-2', 'menu-footer-bottom'); ?>
					<ul class="menu-footer-bottom desktop">

						<li class="current_page_item">
							<a>Netpay no Mundo ></a>
						</li>

					</ul>

				</div>
			</div>
		</div>
	</div>

</footer>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4Y-pJ6_H4riHhOtSWb-44W9tG7uRvMuw&libraries=places&callback=initMap" async defer></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/autocomplete.js"></script>
<script>
	function initMap() {
		if (window.location.pathname.includes('crie-sua-conta')) {
			AUTOCOMPLETE.init(".endereco");
		}

	}
</script>

<?php wp_footer(); ?>

</body>

</html>
$( document ).ready( function () {
	// Hide Header on on scroll down ---
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $( 'header' ).outerHeight() + 15;
	hasScrolled();

	$( window ).scroll( function ( event ) {
		didScroll = true;

		if ( didScroll ) {
			hasScrolled();
			didScroll = false;
		}
	} );


	function hasScrolled() {
		var st = $( this ).scrollTop();

		if ( Math.abs( lastScrollTop - st ) <= delta )
			return;

		if ( st > lastScrollTop && st > navbarHeight ) {
			$( '.header-fixed' ).addClass( 'nav-up slideInDown' ).removeClass( 'slideInUp ' ).show();
		} else {
			// Scroll Up
			if ( st < lastScrollTop && st < navbarHeight ) {
				$( '.header-fixed' ).removeClass( 'nav-up slideInDown' );
			}
		}

		lastScrollTop = st;
	}


	var wpcf7Elm = document.querySelector( '.wpcf7' );
	if ( wpcf7Elm ) {
		wpcf7Elm.addEventListener( 'wpcf7mailsent', function ( event ) {

			switch ( event.target.children[1].name ) {
				case 'crie-sua-conta':
					showMsg( '.form-crie-sua-conta .msg', '.form-crie-sua-conta .o-form' );
					break;
				case 'contact-form-1':
					break;
				default:
					// statements_def
					break;
			}



			console.log( event )

		}, false );
	}

	if ( window.location.hash.includes( '#aba' ) ) {

		$( '.box-4-mouser-over .col-md-4' ).removeClass( 'active' );
		$( '.box-4-mouser-over .col-md-4' ).eq( Number( window.location.hash.split( '#aba' )[1] ) - 1 ).addClass( 'active' );

		$( '.area-cinza .bt_bb_accordion_item' ).removeClass( 'on' );
		$( '.area-cinza .bt_bb_accordion_item' ).eq( Number( window.location.hash.split( '#aba' )[1] ) - 1 ).addClass( 'on' );

		$( "html, body" ).stop().animate( { scrollTop: $( '.box-4-mouser-over' ).offset().top - 100 }, 500, 'swing', function () {

		} );
	}

	$( '.box-4-mouser-over .col-md-4' ).click( function () {
		let index = $( this ).index();

		$( '.box-4-mouser-over .col-md-4' ).removeClass( 'active' );
		$( this ).addClass( 'active' );


		$( '.area-cinza .bt_bb_accordion_item' ).removeClass( 'on' );
		$( '.area-cinza .bt_bb_accordion_item' ).eq( index ).addClass( 'on' );
		$( "html, body" ).stop().animate( { scrollTop: $( '.itens-style-1' ).offset().top - 100 }, 500, 'swing', function () {

		} );



	} );


	$( '.endereco' ).attr( 'autocomplete', 'false' )
	Utils.Masks();

	$( '.field_modalidade' ).find("option").eq(0).text("modalidade")

} )

window.onload = function () {
	window.document.body.style.display = 'block'; // animacao
}

function showMsg( elemShow, elemHide, msg ) {
	if ( elemShow ) document.querySelector( elemShow ).style.display = 'block';
	if ( elemHide ) document.querySelector( elemHide ).style.display = 'none';
	if ( msg ) document.querySelector( elemShow ).innerHTML = msg;
}

var AUTOCOMPLETE = {
	input : null,
	init : function (seletor) {
		let self = this;
		let elem = document.querySelector(seletor);


		this.input = new google.maps.places.Autocomplete((
			elem), {
		});

		this.input.addListener('place_changed', function (id) {
			let place = self.input.getPlace();
			document.querySelector('.num').value = place['address_components'][0]['long_name'] 
			document.querySelector('.cidade').value = place['address_components'][3]['long_name'] 
			document.querySelector('.estado').value = place['address_components'][4]['short_name'] 
			//place['address_components'][0]['long_name'] // number
			//place['address_components'][1]['long_name'] // rua
			//place['address_components'][2]['long_name'] // bairro
			//place['address_components'][3]['long_name'] // cidade
			//place['address_components'][4]['short_name'] // estado
			//place['address_components'][5]['long_name'] // pais
			//place['address_components'][6]['long_name'] // CEP

		});
	}
}
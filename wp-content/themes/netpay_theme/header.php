<!DOCTYPE html>
<html>
<head>
	<title>	<?php wp_title('-',true,'right'); ?> <?php bloginfo('name'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/css/bootstrap.min.css?v11=<?php echo my_rand(); ?>">
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/jquery/jquery-3.3.1.min.js?v11=<?php echo my_rand(); ?>"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/jquery/jquery.mask.min.js?v11=<?php echo my_rand(); ?>"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/js/bootstrap.min.js?v11=<?php echo my_rand(); ?>"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/config.js?v11=<?php echo my_rand(); ?>"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js?v11=<?php echo my_rand(); ?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/includes/fontawesome/css/all.min.css?v11=<?php echo my_rand(); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css?v11=<?php echo my_rand(); ?>">

	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/cropped-icone-32x32.png" sizes="32x32" />
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/cropped-icone-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/img/cropped-icone-180x180.png" />
	<meta name="msapplication-TileImage" content="img/cropped-icone-270x270.png" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139511113-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-139511113-1');
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class('animated fadeIn'); ?>>

	<header>
		<div class="menu-topo">
			<ul class="list-menu-top container-fluid">
				<li>
					<a href="tel:+551135871216">+ 55 (11) 3587-1216</a>
				</li>
				<li>
					<a href="mailto:comercial@netpaybrasil.com.br">comercial@netpaybrasil.com.br</a>
				</li>
			</ul>
		</div>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>
					<a class="navbar-brand logo" href="<?php echo home_url();?>">
						<img class="" src="<?php header_image(); ?>">
					</a>
				</div>
				<div class="collapse navbar-collapse myNavbar" id="myNavbar" >
					<ul class="nav navbar-nav navbar-right user-links">
						<li>
							<a href="https://merchants.netpaybrasil.com.br/WebSite/Login.aspx" target="_blank"> Login<i class="icon-user">
							</i> </a>
						</li>
					</ul>
					
					<?php menu('header-menu','nav navbar-nav navbar-right margin-right-90'); ?>

					<ul class="nav navbar-nav navbar-right user-links">
						<li>
							<a href="<?php echo home_url('crie-sua-conta');?>"  class="botao-3">crie sua conta</a>
						</li>

					</ul>


				</div>
			</div>
		</nav>
		<nav class="navbar navbar-default animated desktop header-fixed">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand logo" href="<?php echo home_url();?>">
						<img class="" src="<?php echo get_template_directory_uri(); ?>/img/logo-transparent.png">
					</a>
				</div>
				<div class="collapse navbar-collapse myNavbar" id="myNavbar2" >
					<ul class="nav navbar-nav navbar-right user-links">
						<li class="no-before">
							<a href="https://merchants.netpaybrasil.com.br/WebSite/Login.aspx" target="_blank"> Login<i class="icon-user">
							</i>
							 </a>
						</li>
					</ul>
				
					<?php menu('header-menu','nav navbar-nav navbar-right margin-right-90'); ?>

					<ul class="nav navbar-nav navbar-right user-links">
						<li>
							<a href="<?php echo home_url('crie-sua-conta');?>"  class="botao-3">crie sua conta </a>
						</li>

					</ul>

				</div>
			</div>
		</nav>
	</header>

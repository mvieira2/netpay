<?php get_header(); ?>

<main>
	<?php if (!is_home() && !is_front_page()): ?>
		<div class="banner-title-page" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/banner4.png') ">
			<h1><?php the_title(); ?></h1>
		</div>

	<?php endif; ?>


<?php get_template_part('loop-page'); ?>

</main>


<?php get_footer(); ?>



<?php get_header(); ?>

<main>

<div class="container-fluidddd" style="
    max-width: 2000px;
    margin: auto;
">
        <div class="row form-contato">
            <div class="col-sm-5">
                <div class="container-fluid">
                <h2>Crie sua conta e seja <br> um parceiro Netpay</h2>
                <p>
                    Após o preenchimento do formulário, você <br>
                    receberá o contato de um de nossos <br>
                    representantes para tirar suar dúvidas e <br>
                    também providenciar a assinatura de nosso <br>
                    contrato de afiliação
                </p>

                <img src="<?php echo get_template_directory_uri(); ?>/img/img2-crie-sua-conta.png">

                </div>
            </div>
            <div class="col-sm-7 ">

                <div class="row form-crie-sua-conta bg-crie-sua-conta">
                <h2>Preencha o formulário abaixo</h2>
                    <?php
                    echo do_shortcode(' [contact-form-7 title="Crie sua conta"]');
                    ?>
                </div>
            </div>
        </div>


    </div>


</main>


<?php get_footer(); ?>
<!-- Contact form 1 -->

<div class="row">
    <div class="col-sm-12">
        [text* your-name placeholder "nome"]
    </div>
    <div class="col-sm-12">
        [email* your-email placeholder "email"]
    </div>
    <div class="col-sm-12">
       [text* your-phone class:tel placeholder "telefone"]
    </div>
    <div class="col-sm-12">
        [text* your-empresa placeholder "empresa"]
    </div>
    <div class="col-sm-12">
         [textarea* your-message "mensagem"]
    </div>
    <div class="col-sm-12 text-right">
        [submit class:botao-4 "enviar"]
    </div>
</div>

<!-- Crie sua conta -->

<div class="row">
    <div class="col-sm-12">
        [text* your-name placeholder "nome"]
    </div>
    <div class="col-sm-12">
        [email* your-email placeholder "email"]
    </div>    
    <div class="col-sm-12">
        [text* your-empresa placeholder "empresa"]
    </div>
    <div class="col-sm-12">
       [text* your-phone class:tel placeholder "telefone com DDD"]
    </div>
    <div class="col-sm-12">
        [text* your-address class:endereco placeholder "endereço"]
    </div>
    <div class="col-sm-8">
        [text* your-city placeholder "cidade"]
    </div>
    <div class="col-sm-4">
        [text* your-state placeholder "estado"]
    </div>
    <div class="col-sm-12">
        [text* your-cnpj class:cnpj placeholder "CNPJ"]
    </div>
    <div class="col-sm-12">
        [select* your-modalidade class:field_modalidade include_blank "Pagamentos à Vista" "Pagamentos Recorrentes" "Pagamentos Parcelados"] 
    </div>
    <div class="col-sm-12 text-right">
        [submit class:botao-4 "enviar"]
    </div>
</div>

<!-- Newsletters -->

<div class="row">
	<div class="col-sm-8">[email* your-email placeholder "digite seu email"]</div>
	<div class="col-sm-4">[submit class:botao-2 "quero receber"]</div>
</div>
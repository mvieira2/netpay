<?php

echo do_shortcode('[bt_bb_section layout="boxed_1200" lazy_load="yes"][bt_bb_row][bt_bb_column lazy_load="yes" width="1/1"][bt_bb_accordion color_scheme="" style="outline" shape="square" closed="" responsive="" publish_datetime="" expiry_datetime="" el_id="" el_class="" el_style=""][bt_bb_accordion_item title=""][bt_bb_text]
<h2>Pagamento à vista</h2>
Permite aos estabelecimentos comerciais oferecer a seus clientes uma forma rápida e segura, online para pagamentos com cartão de crédito à vista.

[/bt_bb_text][/bt_bb_accordion_item][bt_bb_accordion_item title=""][bt_bb_text]
<h2>Pagamentos Recorrentes</h2>
Fazemos todo o gerenciamento das cobranças ao portador do cartão, periodicamente, acionando o estabelecimento comercial apenas para realizar os créditos das transações.

[/bt_bb_text][/bt_bb_accordion_item][bt_bb_accordion_item title=""][bt_bb_text]
<h2>Pagamentos Parcelados</h2>
Permite aos estabelecimentos comerciais oferecer a seus clientes o parcelamento sem juros dos serviços em até 12 vezes.

[/bt_bb_text][/bt_bb_accordion_item][/bt_bb_accordion][/bt_bb_column][/bt_bb_row][/bt_bb_section]');

?>

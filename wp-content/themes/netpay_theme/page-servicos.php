<?php get_header(); ?>

<main>
    <?php if (!is_home() && !is_front_page()) : ?>
        <div class="banner-title-page" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/banner4.png') ">
            <h1><?php the_title(); ?></h1>
        </div>

    <?php endif; ?>
    <br><br>
    <p class="text-center">Através de nosso gateway de pagamento disponibilizamos 3 modalidades de serviço, <br>
        <strong>com certeza uma delas é perfeita para sua empresa!</strong>
    </p>
    <br><br>
    <div class="container">
        <div class="row box-4 box-4-mouser-over">

            <div class="col-md-4 active">
                <div class="box-white">
                    <h3>Pagamentos <br> à vista</h3>
                    <p>
                        Permite aos estabelecimentos <br>
                        comerciais oferecer a seus <br>
                        clientes uma forma rápida e <br>
                        segura, online para pagamentos
                        <br> com cartão de crédito à vista.
                    </p>
                </div>
                <div class="botao-1">
                    <span class="glyphicon glyphicon-menu-down"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box-white">
                    <h3>Pagamentos <br> Recorrentes</h3>
                    <p>
                        Fazemos todo o gerenciamento <br>
                        das cobranças ao portador <br>
                        do cartão, periodicamente, <br>
                        acionando o estabelecimento <br>
                        comercial apenas para realizar <br>
                        os créditos das transações.
                    </p>
                </div>
                <div class="botao-1">
                    <span class="glyphicon glyphicon-menu-down"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box-white">
                    <h3>Pagamentos <br> Parcelados</h3>
                    <p>
                        Permite aos estabelecimentos <br>
                        comerciais oferecer a seus <br>
                        clientes o parcelamento <br>
                        sem juros dos serviços em <br>
                        até 12 vezes.
                    </p>
                </div>
                <div class="botao-1">
                    <span class="glyphicon glyphicon-menu-down"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="area-cinza itens-style-1">
        <div class="center">
            <?php get_template_part('loop-page'); ?>

            <?php //get_template_part('contents/servicos'); ?>

        </div>

    </div>

    <?php get_template_part('contents/principais-parceiros'); ?>


</main>


<?php get_footer(); ?>